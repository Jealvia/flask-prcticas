from flask import Flask,jsonify,request
from products import products

app=Flask(__name__)


@app.route('/products',methods=['GET'])
def getProducts():
    return jsonify({"products":products,"message":"productos"})

@app.route('/products/<string:name>',methods=['GET'])
def getProductsByName(name):
    products_found=[product for product in products if product['name']==name]
    if(len(products_found)>0):
        return jsonify({"products":products_found[0],"message":"searched"})
    else:
        return jsonify({"message":"product not found"})

@app.route('/products',methods=['POST'])
def addProducts():
    new_product={
        "name":request.json['name'],
        "price":request.json['price'],
        "quantity":request.json['quantity']
    }
    products.append(new_product)
    return jsonify({"products":products,"message":"producto agregado"})

@app.route('/products/<string:name>',methods=['PUT'])
def editProductsByName(name):
    products_found=[product for product in products if product['name']==name]
    if(len(products_found)>0):
        products_found[0]['name']=request.json['name']
        products_found[0]['price']=request.json['price']
        products_found[0]['quantity']=request.json['quantity']
        return jsonify({
            "message":"Producto actualizado",
            "products":products
        })
    else:
        return jsonify({"message":"product not found"})

if __name__=='__main__':
    app.run(debug=True)


