from flask import Flask
from flask import request

app=Flask(__name__)

@app.route('/')
def index():
    return 'Hola mundo'

@app.route('/params/')
@app.route('/params/<name>/')
@app.route('/params/<name>/<int:num>/')
def params(name='este es un valor default',num=0):
    return 'El parametro es {} {}'.format(name,num)

app.run(debug=True)
