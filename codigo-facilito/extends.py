from flask import Flask
from flask import render_template

app=Flask(__name__,template_folder='templates/base')

@app.route('/')
def index():
    nombre='Julio'
    return render_template('index.html',name=nombre)

@app.route('/client')
def clientes():
    lista_nombres=['Julio','Andres','Juan']
    return render_template('client.html',lista=lista_nombres)


if __name__=='__main__':
    app.run(debug=True)