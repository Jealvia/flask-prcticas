from flask import Flask
from flask import render_template

app=Flask(__name__,template_folder='templates')

@app.route('/user/<name>')
def index(name='Julio'):
    age=18
    lista=[1,2,3,4,5]
    return render_template('user.html',nombre=name,edad=age,milista=lista)

if __name__=='__main__':
    app.run(debug=True)