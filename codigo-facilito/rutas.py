from flask import Flask
from flask import request

app=Flask(__name__)

@app.route('/')
def index():
    return 'Hola mundo'


@app.route('/params')
def params():
    param=request.args.get('params1','no contiene este parametro')
    param_dos=request.args.get('params2','no contiene este parametro')
    return 'El parametro es {}, {}'.format(param,param_dos)

app.run(debug=True)
